export function omit(obj: { [key: string]: any }, props: string[], fn?: (val: any, key: string, obj: {}) => boolean) {
  if (typeof props === 'function') {
    fn = props;
    props = [];
  }

  if (typeof props === 'string') {
    props = [props];
  }

  const isFunction = typeof fn === 'function';
  const keys = Object.keys(obj);
  const res: any = {};

  // tslint:disable-next-line:prefer-for-of
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const val: any = obj[key];

    if (!props || (props.indexOf(key) === -1 && (!isFunction || !fn || fn(val, key, obj)))) {
      res[key] = val;
    }
  }
  return res;
}
