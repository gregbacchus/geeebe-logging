import * as semver from 'semver';
import { Logger } from './logger';

type Origin = 'uncaughtException' | 'unhandledRejection';

export const monitorUncaughtExceptions = (logger: Logger) => {
  if (semver.satisfies(process.version, '>=13.7.0')) {
    process.on('uncaughtExceptionMonitor' as any, (error: Error, origin: Origin) => {
      logger.fatal(error, { origin });
    });
  } else {
    process.on('uncaughtException' as any, (error: Error, origin: Origin) => {
      logger.fatal(error, { origin });
      process.exit(1);
    });
  }
};
