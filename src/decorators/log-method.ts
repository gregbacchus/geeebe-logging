import { logger } from '../logger';

export function logMethod() {
  return (
    target: any,
    propertyName: string,
    propertyDescriptor: PropertyDescriptor,
  ): PropertyDescriptor => {
    // target === Employee.prototype
    // propertyName === "greet"
    // propertyDescriptor === Object.getOwnPropertyDescriptor(Employee.prototype, "greet")
    const method = propertyDescriptor.value;

    propertyDescriptor.value = function evaluate(...args: any[]) {
      // invoke greet() and get its return value
      try {
        const result = method.apply(this, args);

        logger(`Call: ${propertyName}`, {
          args,
          class: target?.constructor?.name,
          method: propertyName,
          result,
        });

        // return the result of invoking the method
        return result;
      } catch (error) {
        logger(`Call: ${propertyName} => Error`, {
          args,
          class: target?.constructor?.name,
          error: error instanceof Error ? Object.assign({ message: error.message, name: error.name }, error) : error,
          method: propertyName,
        });
        throw error;
      }
    };
    return propertyDescriptor;
  };
}
