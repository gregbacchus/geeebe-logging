import { logger } from '../logger';

export function logProperty(this: any) {
  return (target: object, propertyName: string) => {
    // property value
    let value = this[propertyName];

    // property getter method
    const getter = () => {
      logger(`Get: ${propertyName} => ${value}`, {
        access: 'get',
        class: target?.constructor?.name,
        property: propertyName,
        value,
      });
      return value;
    };

    // property setter method
    const setter = (newVal: any) => {
      logger(`Set: ${propertyName} = ${newVal}`, {
        access: 'set',
        class: target?.constructor?.name,
        newVal,
        oldVal: value,
        property: propertyName,
      });
      value = newVal;
    };

    // Delete property.
    if (delete this[propertyName]) {
      // Create new property with getter and setter
      Object.defineProperty(target, propertyName, {
        configurable: true,
        enumerable: true,
        get: getter,
        set: setter,
      });
    }
  };
}
