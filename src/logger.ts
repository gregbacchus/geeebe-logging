import * as stackTrace from 'stack-trace';
import { ConsoleWriter } from './console-writer';
import { LogWriter } from './log-writer';

import os = require('os');

export interface DebugMeta {
  /** A ticket/reminder should be created to remove Debug logs */
  removalTicket?: string;
}

export interface CriticalMeta {
  code: string;
  /** Ideally the log message/data should include a link to instructions on how to deal with the situation */
  playbookUrl?: string;
}

export interface Logger {
  (message: string, meta?: {}): void;
  verbose(message: string, meta?: {}): void;
  debug<T extends DebugMeta>(message: string, meta?: T): void;
  notice(message: string, meta?: {}): void;
  warn(message: string, error?: Error, meta?: {}): void;
  error(error: Error, meta?: {}): void;
  fatal(error: Error, meta?: {}): void;
  critical<T extends CriticalMeta>(message: string, meta: T): void;
  audit(message: string, meta?: {}): void;
  child(meta: {}): Logger;
}

export interface WithLogger {
  logger: Logger;
}

export interface MaybeWithLogger {
  logger?: Logger;
}

export interface LoggerOptions {
  addStackInfo?: boolean;
}

const DEFAULT_OPTIONS: LoggerOptions = {
  addStackInfo: false,
};

const systemMeta = {
  hostname: os.hostname(),
  pid: process.pid,
};

export const createLogger = (function createLogger(writer: () => LogWriter, meta?: {}, options?: LoggerOptions): Logger {
  const actualOptions = { ...DEFAULT_OPTIONS, ...options };
  const baseMeta = { ...meta };

  const write = (level: string, message: string, meta?: {}): void => {
    const log: any = { message, timestamp: new Date() };
    if (actualOptions.addStackInfo) {
      try {
        // extend data
        const frame = stackTrace.get()[1];
        log.file = frame.getFileName();
        log.line = Number(frame.getLineNumber());

        const type = frame.getTypeName();
        const method = frame.getMethodName();
        Object.assign(log, {
          method: method
            ? `${type}.${method}`
            : frame.getFunctionName(),
        });
      } catch (err) {
        console.error(err);
      }
    }
    writer().write({
      ...log,
      ...systemMeta,
      ...baseMeta,
      ...meta,
      level,
    });
  };

  const clz = ((message: string, meta?: {}): void => write('info', message, meta)) as Logger;
  clz.verbose = (message: string, meta?: {}): void => write('verbose', message, meta);
  clz.debug = (message: string, meta?: {}): void => write('debug', message, meta);
  clz.notice = (message: string, meta?: {}): void => write('notice', message, meta);
  clz.warn = (message: string, error?: Error, meta?: {}): void => write('warn', message, {
    ...error ? { error: `${error.name}: ${error.message}` } : undefined,
    ...error,
    ...meta,
  });
  clz.error = (error: Error, meta?: {}): void => {
    const log = { level: 'error', message: `${error.name}: ${error.message}`, stack: error.stack, timestamp: new Date() };
    writer().write({
      ...log,
      ...error,
      ...systemMeta,
      ...baseMeta,
      ...meta,
    });
  };
  clz.fatal = (error: Error, meta?: {}): void => {
    const log = { level: 'fatal', message: `${error.name}: ${error.message}`, stack: error.stack, timestamp: new Date() };
    writer().write({
      ...log,
      ...error,
      ...systemMeta,
      ...baseMeta,
      ...meta,
    });
  };
  clz.critical = <T extends CriticalMeta>(message: string, meta: T): void => write('warn', message, meta);
  clz.audit = (message: string, meta?: {}): void => write('warn', message, meta);
  clz.child = (meta: {}): Logger => {
    const combinedMeta = { ...baseMeta, ...meta };
    return createLogger(writer, combinedMeta);
  };
  return clz;
});

export class Writer implements LogWriter {
  constructor(private readonly writer: (log: {}) => void) { }

  public write(log: any): void {
    this.writer(log);
  }
}

export const loggerOptions: {
  writer?: LogWriter;
  meta?: {}
} = {};
const consoleWriter = new ConsoleWriter({ pretty: true });
export const logger: Logger = createLogger(
  () => loggerOptions.writer || consoleWriter,
  loggerOptions.meta,
);
